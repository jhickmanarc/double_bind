/*
 * Double Bind template merge.
 *
 * Pass in DOM tree to use as a template.
 * Data is a simple key:value object or
 *      value is an object with properties of 'value' for the new value string,
 *      'classes' for classes that could be used and 'applied_classes' for classes to be applied.
 * Merges into elements with a class named __bind_<var name>
 * Returns duplicate of the DOM tree passed in.
 */
function dom_merge(template_el, data, duplicate) {
    function apply(el, d) {
        if (typeof d === 'object' && d !== null) {
            var value = d.value;
            // Process class updates
            if (d.hasOwnProperty('classes')) {
                for (var i = 0; i < d.classes.length; i++) {
                    el.classList.remove(d.classes[i]);
                }
            }
            if (d.hasOwnProperty('applied_classes')) {
                for (var i = 0; i < d.applied_classes.length; i++) {
                    el.classList.add(d.applied_classes[i]);
                }
            }
            if (d.hasOwnProperty('props')) {
                for (prop in d.props) {
                    if (d.props.hasOwnProperty(prop)) {
                        el.setAttribute(prop, d.props[prop]);
                    }
                }
            }
            if (d.hasOwnProperty('events')) {
                for (var i = 0; i < d.events.length; i++) {
                    el.addEventListener(d.events[i].event, d.events[i].handler);
                }
            }
        }
        else {
            var value = d;
        }
        switch(el.tagName) {
            case 'INPUT':
            case 'TEXTAREA':
            case 'SELECT':
                if (el.type == "checkbox") {
                    el.checked = value;
                }
                else {
                    el.value = value;
                }
                break;
            default:
                if (value !== undefined) {
                    el.innerHTML = value;
                }
        }
    }

    if (duplicate === true) {
        var node = template_el.cloneNode(true);
    }
    else {
        var node = template_el;
    }
    for (variable in data) {
         if (data.hasOwnProperty(variable)) {
            var bind_class = '__bind_' + variable;
            var bind_els = node.querySelectorAll('.' + bind_class);
            if (node.classList[0] == bind_class) {
                apply(node, data[variable]);
            }
            for (var i = 0; i < bind_els.length; i++) {
                apply(bind_els[i], data[variable]);
            }
        }
    }
    return node;
}