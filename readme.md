# Double Bind DOM templating library

Simple light templating operation based on browser DOM structures.

2019 Created by James Hickman <james@jameshickman.net> Released under GNU LGPL V3.

Usage:
```
dom_merge(
    template_el: <DOM pointer>,
    data: <data_object>,
    duplicate: <true|false>
);
```
## Template subtree
DOM pointer to a sub-tree within the DOM to update or use as a template

## Data structure to merge
The properties of object members are the names of the variables being merged in. Any DOM element where the first class is names '__bind_<variable_name>' is populated with the value.

If the data value is a string that is injected as innerHTML or form element value.

If the value provided is an object the members can be used to set value, set event handlers, change classes and set custom attributes as specified below:
```
{
	'value': 'string data',                     // Value to be injected as innerHTML or value of a form element
	'classes': ['class_name', ...],             // Classes to REMOVE from the element
	'applied_classes': ['class_name', ...],     // Classes to ADD to element
	'props': {                                  // Add custom properties to element
		'property_name':'value',
		...
	},
	'events': [                                 // Apply events to the element, array of arrays where the first element is event name and second is funciton
		['event_type', function],
		...
	]
}
```
## Source duplication
If the last parameter is true the source DOM sub-tree is duplicated and data injected into the copy.
If false the data is injected directly into the passed subtree.
## Return value
The pointer to the sub-tree the data was injected into. If duplicating that is the new copy with data injected else the origional.